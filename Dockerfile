FROM docker:dind

ENV BUILD_DEPS="gettext"  \
    RUNTIME_DEPS="libintl"

RUN set -x && \
    apk add --update $RUNTIME_DEPS && \
    apk add --virtual build_deps $BUILD_DEPS &&  \
    cp /usr/bin/envsubst /usr/local/bin/envsubst && \
    apk del build_deps
RUN apk add bash python3 py3-pip

COPY entrypoint.sh entrypoint.sh 
RUN chmod +x entrypoint.sh

RUN pip install https://github.com/lion2486/portainer-cli/archive/refs/heads/master.zip

ENTRYPOINT ["/entrypoint.sh"]
CMD [ "/bin/bash" ]
