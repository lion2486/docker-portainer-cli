# Docker portainer-cli



## Usage

docker pull registry.gitlab.com/lion2486/docker-portainer-cli
docker run registry.gitlab.com/lion2486/docker-portainer-cli

## Use in the CI
TODO

## Environment Variables

- PORTAINER_URL         required
- PORTAINER_API_KEY     optional, recommended
- PORTAINER_USER        optional
- PORTAINER_PASSWORD    optional
