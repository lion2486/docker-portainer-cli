#!/bin/bash

portainer-cli configure $PORTAINER_URL

if [[ -z "${PORTAINER_API_KEY}" ]]; then
  portainer-cli login $PORTAINER_USER $PORTAINER_PASSWORD
else
  portainer-cli set_apikey $PORTAINER_API_KEY
fi

exec "${@}"
